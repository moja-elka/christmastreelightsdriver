/** Note defintions */
#define NOTE_B0  31
#define NOTE_C1  33
#define NOTE_CS1 35
#define NOTE_D1  37
#define NOTE_DS1 39
#define NOTE_E1  41
#define NOTE_F1  44
#define NOTE_FS1 46
#define NOTE_G1  49
#define NOTE_GS1 52
#define NOTE_A1  55
#define NOTE_AS1 58
#define NOTE_B1  62
#define NOTE_C2  65
#define NOTE_CS2 69
#define NOTE_D2  73
#define NOTE_DS2 78
#define NOTE_E2  82
#define NOTE_F2  87
#define NOTE_FS2 93
#define NOTE_G2  98
#define NOTE_GS2 104
#define NOTE_A2  110
#define NOTE_AS2 117
#define NOTE_B2  123
#define NOTE_C3  131
#define NOTE_CS3 139
#define NOTE_D3  147
#define NOTE_DS3 156
#define NOTE_E3  165
#define NOTE_F3  175
#define NOTE_FS3 185
#define NOTE_G3  196
#define NOTE_GS3 208
#define NOTE_A3  220
#define NOTE_AS3 233
#define NOTE_B3  247
#define NOTE_C4  262
#define NOTE_CS4 277
#define NOTE_D4  294
#define NOTE_DS4 311
#define NOTE_E4  330
#define NOTE_F4  349
#define NOTE_FS4 370
#define NOTE_G4  392
#define NOTE_GS4 415
#define NOTE_A4  440
#define NOTE_AS4 466
#define NOTE_B4  494
#define NOTE_C5  523
#define NOTE_CS5 554
#define NOTE_D5  587
#define NOTE_DS5 622
#define NOTE_E5  659
#define NOTE_F5  698
#define NOTE_FS5 740
#define NOTE_G5  784
#define NOTE_GS5 831
#define NOTE_A5  880
#define NOTE_AS5 932
#define NOTE_B5  988
#define NOTE_C6  1047
#define NOTE_CS6 1109
#define NOTE_D6  1175
#define NOTE_DS6 1245
#define NOTE_E6  1319
#define NOTE_F6  1397
#define NOTE_FS6 1480
#define NOTE_G6  1568
#define NOTE_GS6 1661
#define NOTE_A6  1760
#define NOTE_AS6 1865
#define NOTE_B6  1976
#define NOTE_C7  2093
#define NOTE_CS7 2217
#define NOTE_D7  2349
#define NOTE_DS7 2489
#define NOTE_E7  2637
#define NOTE_F7  2794
#define NOTE_FS7 2960
#define NOTE_G7  3136
#define NOTE_GS7 3322
#define NOTE_A7  3520
#define NOTE_AS7 3729
#define NOTE_B7  3951
#define NOTE_C8  4186
#define NOTE_CS8 4435
#define NOTE_D8  4699
#define NOTE_DS8 4978
#define REST     0

/** Melodies */
//1 - Cicha noc
const int tempo1 = 140;
const int melody1[] PROGMEM = {
  NOTE_G4,-4, NOTE_A4,8, NOTE_G4,4,
  NOTE_E4,-2,
  NOTE_G4,-4, NOTE_A4,8, NOTE_G4,4,
  NOTE_E4,-2,
  NOTE_D5,2, NOTE_D5,4,
  NOTE_B4,-2,
  NOTE_C5,2, NOTE_C5,4,
  NOTE_G4,-2,

  NOTE_A4,2, NOTE_A4,4,
  NOTE_C5,-4, NOTE_B4,8, NOTE_A4,4,
  NOTE_G4,-4, NOTE_A4,8, NOTE_G4,4,
  NOTE_E4,-2,
  NOTE_A4,2, NOTE_A4,4,
  NOTE_C5,-4, NOTE_B4,8, NOTE_A4,4,
  NOTE_G4,-4, NOTE_A4,8, NOTE_G4,4,
  NOTE_E4,-2,

  NOTE_D5,2, NOTE_D5,4,
  NOTE_F5,-4, NOTE_D5,8, NOTE_B4,4,
  NOTE_C5,-2,
  NOTE_E5,-2,
  NOTE_C5,4, NOTE_G4,4, NOTE_E4,4,
  NOTE_G4,-4, NOTE_F4,8, NOTE_D4,4,
  NOTE_C4,-2,
  NOTE_C4,-1,
};

//2 - Przybie�eli do Betlejem Pasterze
const int tempo2 = 100;
const int melody2[] PROGMEM = {
    NOTE_C4,8, NOTE_B3,8, NOTE_C4,8, NOTE_D4,8,
    NOTE_E4,8, NOTE_D4,8, NOTE_E4,8, NOTE_F4,8,
    NOTE_G4,4, NOTE_A4,4,
    NOTE_G4,2,

    NOTE_C4,8, NOTE_B3,8, NOTE_C4,8, NOTE_D4,8,
    NOTE_E4,8, NOTE_D4,8, NOTE_E4,8, NOTE_F4,8,
    NOTE_G4,4, NOTE_A4,4,
    NOTE_G4,2,

    NOTE_C5,4, NOTE_G4,8, NOTE_G4,8,
    NOTE_A4,8, NOTE_G4,8, NOTE_F4,8, NOTE_E4,8,
    NOTE_F4,4, NOTE_F4,8, NOTE_F4,8,
    NOTE_G4,8, NOTE_F4,8, NOTE_E4,8, NOTE_D4,8,

    NOTE_E4,4, NOTE_F4,4,
    NOTE_G4,2,
    NOTE_E4,4, NOTE_D4,4,
    NOTE_C4,2,

    NOTE_C5,4, NOTE_G4,8, NOTE_G4,8,
    NOTE_A4,8, NOTE_G4,8, NOTE_F4,8, NOTE_E4,8,
    NOTE_F4,4, NOTE_F4,8, NOTE_F4,8,
    NOTE_G4,8, NOTE_F4,8, NOTE_E4,8, NOTE_D4,8,

    NOTE_E4,4, NOTE_F4,4,
    NOTE_G4,2,
    NOTE_E4,4, NOTE_D4,4,
    NOTE_C4,2,
};

//3 - P�jd�my wszyscy do stajenki
const int tempo3 = 140;
const int melody3[] PROGMEM = {
    NOTE_D4,4,  NOTE_E4,4, NOTE_D4,4, NOTE_E4,4,
    NOTE_D4,-4, NOTE_G4,8, NOTE_B4,4, NOTE_G4,4,
    NOTE_D4,4,  NOTE_E4,4, NOTE_D4,4, NOTE_E4,4,
    NOTE_D4,-4, NOTE_G4,8, NOTE_B4,4, NOTE_G4,4,

    NOTE_C5,-4, NOTE_B4,8, NOTE_A4,4, NOTE_A4,4,
    NOTE_B4,-4, NOTE_A4,8, NOTE_G4,4, NOTE_G4,4,

    NOTE_FS4,4,  NOTE_FS4,4, NOTE_E4,4, NOTE_FS4,4,
    NOTE_G4,-4, NOTE_A4,8, NOTE_B4,4, NOTE_B4,4,

    NOTE_C5,-4, NOTE_B4,8, NOTE_A4,4, NOTE_A4,4,
    NOTE_B4,-4, NOTE_A4,8, NOTE_G4,4, NOTE_G4,4,

    NOTE_FS4,4,  NOTE_FS4,4, NOTE_E4,4, NOTE_FS4,4,
    NOTE_G4,-4, NOTE_G4,8, NOTE_G4,4, NOTE_G4,4,
};

//4 - W�r�d nocnej ciszy
const int tempo4 = 140;
const int melody4[] PROGMEM = {
    NOTE_G4,2, NOTE_A4,4, NOTE_FS4,4, NOTE_G4,2, NOTE_D4,2,
    NOTE_B4,4, NOTE_B4,4, NOTE_C5,4, NOTE_A4,4, NOTE_B4,2, REST,2,
    NOTE_G4,2, NOTE_A4,4, NOTE_FS4,4, NOTE_G4,2, NOTE_D4,2,
    NOTE_B4,4, NOTE_B4,4, NOTE_C5,4, NOTE_A4,4, NOTE_B4,2, REST,2,

    NOTE_G4,4, NOTE_B4,4, NOTE_G4,4, NOTE_B4,4, NOTE_C5,-4, NOTE_A4,8, NOTE_FS4,4, NOTE_D4,4,
    NOTE_G4,4, NOTE_B4,4, NOTE_G4,4, NOTE_B4,4, NOTE_C5,-4, NOTE_A4,8, NOTE_FS4,4, NOTE_D4,4,

    NOTE_G4,4, NOTE_G4,4, NOTE_A4,4, NOTE_A4,4, NOTE_B4,1,
    NOTE_G4,4, NOTE_G4,4, NOTE_A4,4, NOTE_A4,4, NOTE_G4,1,
};

//5 - Dzisiaj w Betlejem
const int tempo5 = 110;
const int melody5[] PROGMEM = {
    NOTE_F4,4, NOTE_F4,8, NOTE_C4,8, NOTE_F4,8, NOTE_G4,8,
    NOTE_A4,4, NOTE_A4,8, NOTE_G4,8, NOTE_A4,8, NOTE_B4,8,
    NOTE_C5,8, NOTE_D5,8, NOTE_C5,4, NOTE_B4,4, NOTE_A4,4, NOTE_G4,2, REST,4,

    NOTE_F4,4, NOTE_F4,8, NOTE_C4,8, NOTE_F4,8, NOTE_G4,8,
    NOTE_A4,4, NOTE_A4,8, NOTE_G4,8, NOTE_A4,8, NOTE_B4,8,
    NOTE_C5,8, NOTE_D5,8, NOTE_C5,4, NOTE_B4,4, NOTE_A4,4, NOTE_G4,2, REST,4,

    NOTE_C5,4, NOTE_C5,8, NOTE_B4,8, NOTE_A4,8, NOTE_G4,8,
    NOTE_F4,4, NOTE_F4,8, NOTE_C4,8, NOTE_F4,8, NOTE_A4,8,

    NOTE_C5,4, NOTE_C5,8, NOTE_B4,8, NOTE_A4,8, NOTE_G4,8,
    NOTE_F4,4, NOTE_F4,8, NOTE_C4,8, NOTE_F4,8, NOTE_A4,8,

    NOTE_C5,8, NOTE_D5,8, NOTE_C5,8, NOTE_B4,8, NOTE_A4,8, NOTE_B4,8,
    NOTE_C5,8, NOTE_D5,8, NOTE_C5,8, NOTE_B4,8, NOTE_A4,8, NOTE_B4,8,

    NOTE_C5,8, NOTE_C5,8, NOTE_D5,4, NOTE_C5,4,
    NOTE_B4,4, NOTE_A4,4, NOTE_G4,4, NOTE_F4,2, REST,4,
};

//6 - B�g si� rodzi
const int tempo6 = 100;
const int melody6[] PROGMEM = {
    NOTE_C4,8, NOTE_C5,8, NOTE_C5,4, NOTE_B4,8, NOTE_A4,8,
    NOTE_A4,8, NOTE_A4,8, NOTE_A4,4, NOTE_G4,8, NOTE_F4,8,
    NOTE_E4,8, NOTE_G4,8, NOTE_C5,8, NOTE_G4,8, NOTE_F4,8, NOTE_E4,8, NOTE_E4,4, NOTE_D4,4,

    NOTE_C4,8, NOTE_C5,8, NOTE_C5,4, NOTE_B4,8, NOTE_A4,8,
    NOTE_A4,8, NOTE_A4,8, NOTE_A4,4, NOTE_G4,8, NOTE_F4,8,
    NOTE_E4,8, NOTE_G4,8, NOTE_C5,8, NOTE_G4,8, NOTE_F4,8, NOTE_E4,8, NOTE_E4,4, NOTE_D4,4,

    NOTE_F4,-4, NOTE_D4,8, NOTE_E4,8, NOTE_F4,8, NOTE_G4,8, NOTE_G4,8, NOTE_E4,4, NOTE_G4,4,
    NOTE_F4,-4, NOTE_D4,8, NOTE_E4,8, NOTE_F4,8, NOTE_G4,8, NOTE_G4,8, NOTE_E4,4, NOTE_G4,4,

    NOTE_F4,-4, NOTE_D4,8, NOTE_E4,8, NOTE_F4,8, NOTE_G4,8, NOTE_G4,8, NOTE_E4,4, NOTE_G4,4,
    NOTE_F4,-4, NOTE_D4,8, NOTE_E4,8, NOTE_F4,8, NOTE_G4,-8, NOTE_B3,-8, NOTE_D4,4, NOTE_C4,4,
};

//7 - Gdy si� Chrystus rodzi
const int tempo7 = 100;
const int melody7[] PROGMEM = {
    NOTE_G4,4, NOTE_E4,4, NOTE_C5,4, NOTE_A4,4, NOTE_A4,2, NOTE_G4,2,
    NOTE_E4,4, NOTE_E4,4, NOTE_F4,4, NOTE_D4,4, NOTE_D4,2, NOTE_C4,2,
    NOTE_G4,4, NOTE_E4,4, NOTE_C5,4, NOTE_A4,4, NOTE_A4,2, NOTE_G4,2,
    NOTE_E4,4, NOTE_E4,4, NOTE_F4,4, NOTE_D4,4, NOTE_D4,2, NOTE_C4,2,

    NOTE_D4,4, NOTE_D4,8, NOTE_E4,8, NOTE_F4,4, NOTE_D4,4,
    NOTE_E4,4, NOTE_E4,8, NOTE_F4,8, NOTE_G4,4, NOTE_E4,4,
    NOTE_D4,4, NOTE_D4,8, NOTE_E4,8, NOTE_F4,4, NOTE_D4,4,
    NOTE_E4,4, NOTE_E4,8, NOTE_F4,8, NOTE_G4,4, NOTE_E4,4,

    NOTE_G4,2, NOTE_G4,2, NOTE_A4,2, NOTE_A4,2, NOTE_B4,2, NOTE_B4,2,
    NOTE_C5,4, NOTE_G4,4, NOTE_A4,4, NOTE_F4,4, NOTE_E4,2, NOTE_D4,2, NOTE_C4,2, REST,2,
};

//8 - Do szopy, hej pasterze
const int tempo8 = 100;
const int melody8[] PROGMEM = {
    NOTE_D4,4, NOTE_G4,4, NOTE_G4,4, NOTE_A4,8, NOTE_G4,8, NOTE_FS4,4, NOTE_FS4,4,
    NOTE_D4,4, NOTE_A4,4, NOTE_A4,4, NOTE_B4,8, NOTE_A4,8, NOTE_G4,2,

    NOTE_D4,4, NOTE_G4,4, NOTE_G4,4, NOTE_A4,8, NOTE_G4,8, NOTE_FS4,4, NOTE_FS4,4,
    NOTE_D4,4, NOTE_A4,4, NOTE_A4,4, NOTE_B4,8, NOTE_A4,8, NOTE_G4,2,

    NOTE_D4,4, NOTE_B4,4, NOTE_B4,4, NOTE_C5,8, NOTE_B4,8, NOTE_A4,4, NOTE_A4,4,
    NOTE_C5,4, NOTE_C5,4, NOTE_E5,4, NOTE_D5,8, NOTE_C5,8, NOTE_D5,2,

    NOTE_D4,4, NOTE_B4,4, NOTE_B4,4, NOTE_C5,8, NOTE_B4,8, NOTE_A4,4, NOTE_A4,4,
    NOTE_D4,4, NOTE_A4,4, NOTE_A4,4, NOTE_B4,8, NOTE_A4,8, NOTE_G4,2,
};

//9 - Jezus malusie�ki
const int tempo9 = 90;
const int melody9[] PROGMEM = {
    NOTE_C5,8, NOTE_B4,8, NOTE_A4,4, NOTE_GS4,8, NOTE_A4,8, NOTE_B4,4, NOTE_E4,2,
    NOTE_C5,8, NOTE_B4,8, NOTE_A4,4, NOTE_GS4,8, NOTE_A4,8, NOTE_B4,4, NOTE_E4,2,

    NOTE_D4,8, NOTE_E4,8, NOTE_F4,4, NOTE_F4,4,
    NOTE_E4,8, NOTE_F4,8, NOTE_G4,4, NOTE_G4,4,
    NOTE_A4,8, NOTE_G4,8, NOTE_F4,4, NOTE_E4,8, NOTE_D4,8, NOTE_D4,4, NOTE_E4,2,

    NOTE_D4,8, NOTE_E4,8, NOTE_F4,4, NOTE_F4,4,
    NOTE_E4,8, NOTE_F4,8, NOTE_G4,4, NOTE_G4,4,
    NOTE_A4,8, NOTE_G4,8, NOTE_F4,4, NOTE_E4,8, NOTE_D4,8, NOTE_D4,4, NOTE_C4,2,
};

//10 - W ��obie le�y
const int tempo10 = 90;
const int melody10[] PROGMEM = {
    NOTE_C4,8, NOTE_C4,8, NOTE_F4,8, NOTE_G4,8, NOTE_A4,4,
    NOTE_G4,8, NOTE_F4,8, NOTE_G4,8, NOTE_A4,8, NOTE_AS4,4,
    NOTE_A4,8, NOTE_AS4,8, NOTE_C5,4, NOTE_AS4,4,
    NOTE_A4,8, NOTE_G4,8, NOTE_F4,2,

    NOTE_C4,8, NOTE_C4,8, NOTE_F4,8, NOTE_G4,8, NOTE_A4,4,
    NOTE_G4,8, NOTE_F4,8, NOTE_G4,8, NOTE_A4,8, NOTE_AS4,4,
    NOTE_A4,8, NOTE_AS4,8, NOTE_C5,4, NOTE_AS4,4,
    NOTE_A4,8, NOTE_G4,8, NOTE_F4,2,

    NOTE_C5,-4,  NOTE_AS4,8, NOTE_A4,8,  NOTE_A4,8,
    NOTE_D5,8,   NOTE_C5,8,  NOTE_AS4,8, NOTE_A4,8, NOTE_AS4,4,

    NOTE_AS4,-4, NOTE_A4,8,  NOTE_G4,8,  NOTE_G4,8,
    NOTE_C5,8,   NOTE_AS4,8, NOTE_A4,8,  NOTE_G4,8, NOTE_A4,4,

    NOTE_F4,8, NOTE_E4,8, NOTE_D4,4, NOTE_AS4,4,
    NOTE_A4,8, NOTE_G4,8, NOTE_F4,2,
};

/** Pins */
const int PIN_BUZZER = 6;
const int PIN_LED = 13;
const int PIN_BUTTON_COMMON = 2;
const int PIN_BUTTON_GREEN = 3;
const int PIN_BUTTON_YELLOW = 4;
const int PIN_BUTTON_RED = 5;

/** Device modes */
const int MODE_BLINKER = 1;
const int MODE_CAROLS = 2;

/** Blinker mode configuration */
const unsigned long BLINKER_FLASH_DURATION = 100; //ms
const unsigned long BLINKER_DEFAULT_DURATION = 500;  //ms
const unsigned long BLINKER_DURATION_STEP = 500; //ms
const unsigned long BLINKER_MAX_DURATION = 3000;  //ms
const int BLINKER_OPTIONS_COUNT = 6;
int currentBlinkerOption = 0;
int lastBlinkerMode = 0;
unsigned long currentBlinkerDuration = BLINKER_DEFAULT_DURATION;  //ms

/** Carols mode configuration */
const int CAROLS_OPTIONS_COUNT = 2; //UNMUTE, MUTE
const int CAROLS_OPTION_UNMUTE = 0;
const int CAROLS_OPTION_MUTE = 1;
int currentCarolsOption = CAROLS_OPTION_UNMUTE; //UNMUTE
const unsigned long CAROLS_LED_DEFAULT_TIME_SUBSTRACTION = 50; //ms
const unsigned long CAROLS_LED_TIME_SUBSTRACTION_STEP = 50; //ms
const unsigned long CAROLS_LED_MAX_TIME_SUBSTRACTION = 300; //ms
unsigned long carolsLedTimeSubstraction = 0;

/** Led configuration */
unsigned long lastLedOnTime = 0;
unsigned long ledOnTime = 0;
int isLedOn = 0;
int timer1Counter;

/** Buttons configuration */
unsigned long lastButtonPressTime = 0;
unsigned long debounceTime = 250;

/** Current device configuration */
int deviceMode = MODE_BLINKER;

void setup()
{
    pinMode(PIN_BUZZER, OUTPUT);
    pinMode(PIN_LED, OUTPUT);
    buttonsSetup();
    timerSetup();
}

void buttonsSetup()
{
    configureButtonsCommon();
    attachInterrupt(digitalPinToInterrupt(PIN_BUTTON_COMMON), pressButtonInterrupt, FALLING);
}

void timerSetup()
{
    noInterrupts();
    TCCR1A = 0;
    TCCR1B = 0;
    // Set timer1_counter to the correct value for our interrupt interval
    //timer1Counter = 64911;   // preload timer 65536-16MHz/256/100Hz
    timer1Counter = 64286;   // preload timer 65536-16MHz/256/50Hz
    //timer1Counter = 34286;   // preload timer 65536-16MHz/256/2Hz

    TCNT1 = timer1Counter;   // preload timer
    TCCR1B |= (1 << CS12);    // 256 prescaler
    TIMSK1 |= (1 << TOIE1);   // enable timer overflow interrupt
    interrupts();             // enable all interrupts
}

ISR(TIMER1_OVF_vect)
{
    TCNT1 = timer1Counter;   // preload timer

    if (MODE_CAROLS == deviceMode && millis() - lastLedOnTime > ledOnTime) {
        digitalWrite(PIN_LED, LOW);
    }
}

void configureButtonsCommon()
{
    pinMode(PIN_BUTTON_COMMON, INPUT_PULLUP);
    pinMode(PIN_BUTTON_GREEN, OUTPUT);
    pinMode(PIN_BUTTON_YELLOW, OUTPUT);
    pinMode(PIN_BUTTON_RED, OUTPUT);
    digitalWrite(PIN_BUTTON_GREEN, LOW);
    digitalWrite(PIN_BUTTON_YELLOW, LOW);
    digitalWrite(PIN_BUTTON_RED, LOW);
}

void configureButtonsDistinct()
{
    pinMode(PIN_BUTTON_COMMON, OUTPUT);
    digitalWrite(PIN_BUTTON_COMMON, LOW);
    pinMode(PIN_BUTTON_GREEN, INPUT_PULLUP);
    pinMode(PIN_BUTTON_YELLOW, INPUT_PULLUP);
    pinMode(PIN_BUTTON_RED, INPUT_PULLUP);
}

void pressButtonInterrupt()
{
    if (millis() - lastButtonPressTime < debounceTime) {
        return;
    }

    lastButtonPressTime = millis();
    configureButtonsDistinct();

    if (!digitalRead(PIN_BUTTON_GREEN)) {
       deviceMode = MODE_BLINKER == deviceMode ? MODE_CAROLS : MODE_BLINKER;
    }

    if (!digitalRead(PIN_BUTTON_YELLOW)) {
        if (MODE_BLINKER == deviceMode) {
            currentBlinkerOption = (currentBlinkerOption < BLINKER_OPTIONS_COUNT - 1) ? currentBlinkerOption + 1 : 0;
        } else if (MODE_CAROLS == deviceMode) {
            currentCarolsOption = (CAROLS_OPTION_UNMUTE == currentCarolsOption) ? CAROLS_OPTION_MUTE : CAROLS_OPTION_UNMUTE;
        }
    }

    if (!digitalRead(PIN_BUTTON_RED)) {
        if (MODE_BLINKER == deviceMode) {
            currentBlinkerDuration = (currentBlinkerDuration < BLINKER_MAX_DURATION) ? currentBlinkerDuration + BLINKER_DURATION_STEP : BLINKER_DEFAULT_DURATION;
        } else if (MODE_CAROLS == deviceMode) {
            carolsLedTimeSubstraction = (carolsLedTimeSubstraction < CAROLS_LED_MAX_TIME_SUBSTRACTION) ? carolsLedTimeSubstraction + CAROLS_LED_TIME_SUBSTRACTION_STEP : CAROLS_LED_DEFAULT_TIME_SUBSTRACTION;
        }
    }

    configureButtonsCommon();
}

void loop()
{
    if (deviceMode == MODE_BLINKER) {
        runBlinkerMode();
    } else if (deviceMode == MODE_CAROLS) {
        runCarolsMode();
    }
}

void runBlinkerMode()
{
    if (currentBlinkerOption != lastBlinkerMode) {
        digitalWrite(PIN_LED, LOW);
        isLedOn = 0;
        lastBlinkerMode = currentBlinkerOption;
    }

    switch (currentBlinkerOption) {
        case 0:
            if (millis() - lastLedOnTime > BLINKER_FLASH_DURATION) {
                digitalWrite(PIN_LED, !digitalRead(PIN_LED));
                lastLedOnTime = millis();
            }
            break;
        case 1:
            toggleLed(BLINKER_FLASH_DURATION, currentBlinkerDuration);
            break;
        case 2:
            toggleLed(currentBlinkerDuration, currentBlinkerDuration);
            break;
        case 3:
            toggleLed(currentBlinkerDuration, 2 * currentBlinkerDuration);
            break;
        case 4:
            toggleLed(currentBlinkerDuration, 3 * currentBlinkerDuration);
            break;
        case 5:
            toggleLed(currentBlinkerDuration, 4 * currentBlinkerDuration);
            break;
    }
}

void toggleLed(unsigned long turnOnTime, unsigned long turnOffTime)
{
    if (isLedOn == 1 && millis() - lastLedOnTime > turnOnTime) {
        digitalWrite(PIN_LED, !digitalRead(PIN_LED));
        lastLedOnTime = millis();
        isLedOn = 0;
    } else if (isLedOn == 0 && millis() - lastLedOnTime > turnOffTime) {
        digitalWrite(PIN_LED, !digitalRead(PIN_LED));
        lastLedOnTime = millis();
        isLedOn = 1;
    }
}

void runCarolsMode()
{
    playMelody(melody1, tempo1, sizeof(melody1)/4);
    playMelody(melody2, tempo2, sizeof(melody2)/4);
    playMelody(melody3, tempo3, sizeof(melody3)/4);
    playMelody(melody4, tempo4, sizeof(melody4)/4);
    playMelody(melody5, tempo5, sizeof(melody5)/4);
    playMelody(melody6, tempo6, sizeof(melody6)/4);
    playMelody(melody7, tempo7, sizeof(melody7)/4);
    playMelody(melody8, tempo8, sizeof(melody8)/4);
    playMelody(melody9, tempo9, sizeof(melody9)/4);
    playMelody(melody10, tempo10, sizeof(melody10)/4);
}

void playMelody(const int melody[], int tempo, int notesCount)
{
    if (MODE_BLINKER == deviceMode) {
        return;
    }

    int wholeNote = (60000 * 4) / tempo;
    int divider = 0, noteDuration = 0;

    for (int thisNote = 0; thisNote < notesCount * 2; thisNote = thisNote + 2) {
        if (MODE_BLINKER == deviceMode) {
            break;
        }

        divider = pgm_read_word_near(melody + thisNote + 1);

        if (divider > 0) {
            noteDuration = (wholeNote) / divider;
        } else if (divider < 0) {
            noteDuration = (wholeNote) / abs(divider);
            noteDuration *= 1.5;
        }

        turnOnLed(noteDuration);

        if (currentCarolsOption == 0) {
            tone(PIN_BUZZER, pgm_read_word_near(melody + thisNote), noteDuration * 0.9);
            delay(noteDuration);
            noTone(PIN_BUZZER);
        } else if (currentCarolsOption == 0) {
            delay(noteDuration);
        }
    }

    delay(1000);
}

void turnOnLed(int duration)
{
    if ((unsigned long) duration < CAROLS_LED_DEFAULT_TIME_SUBSTRACTION + carolsLedTimeSubstraction) {
        return;
    }

    digitalWrite(PIN_LED, HIGH);
    lastLedOnTime = millis();
    ledOnTime = duration - CAROLS_LED_DEFAULT_TIME_SUBSTRACTION - carolsLedTimeSubstraction;
}
